import 'package:flutter/material.dart';

class GameView extends StatefulWidget {
  const GameView({super.key});

  @override
  State<GameView> createState() => _GameViewState();
}

class _GameViewState extends State<GameView> {
  static const String playerX = 'X';
  static const String playerO = 'O';

  late String currentPlayer;
  late bool gameFinished;
  late List<String> busy;

  @override
  void initState() {
    startGame();
    super.initState();
  }

  void startGame() {
    currentPlayer = playerX;
    gameFinished = false;
    busy = ['', '', '', '', '', '', '', '', ''];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _headerText(),
            _gameContainer(),
          ],
        ),
      ),
    );
  }

  Widget _headerText() {
    return SafeArea(
      child: Column(
        children: [
          const Text(
            'Triqui Demo',
            style: TextStyle(
              color: Colors.deepPurple,
              fontWeight: FontWeight.bold,
              fontSize: 32,
            ),
          ),
          Text(
            '$currentPlayer turn',
            style: const TextStyle(
              color: Colors.black54,
              fontSize: 24,
            ),
          ),
        ],
      ),
    );
  }

  Widget _gameContainer() {
    return Container(
      height: MediaQuery.of(context).size.height / 2,
      width: MediaQuery.of(context).size.width,
      margin: const EdgeInsets.all(8),
      child: GridView.builder(
        gridDelegate:
            const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
        itemCount: 9,
        itemBuilder: (context, int index) {
          return _box(index);
        },
      ),
    );
  }

  Widget _box(int index) {
    return InkWell(
      onTap: () {
        if (gameFinished || busy[index].isNotEmpty) {
          return; //validación de no repetir
        }

        setState(() {
          busy[index] = currentPlayer;
          changeTurn();
        });
      },
      child: Container(
        color: busy[index].isEmpty
            ? Colors.black26
            : busy[index] == playerX
                ? Colors.deepPurple[100]
                : Colors.deepPurple[300],
        margin: const EdgeInsets.all(8),
        child: Center(
          child: Text(
            busy[index],
            style: const TextStyle(fontSize: 40),
          ),
        ),
      ),
    );
  }

  changeTurn() {
    if (currentPlayer == playerX) {
      currentPlayer = playerO;
    } else {
      currentPlayer = playerX;
    }
  }
}
